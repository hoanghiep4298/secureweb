let db = require('../database/db_connection');
const bcrypt = require('bcrypt');
const saltRound = 10;
module.exports.postLogin = async function(req, res){
    let regex = /^[a-z1-9]*@([\w\d\.]*)/g
    let result = {};
    if(!regex.test(req.body.username)){
        result.error = "DON'T try to hack"
        return result;
    }
    let sql = `select * from users where username = \'${req.body.username}\'`
    console.log(sql)
    let user =  await db.getUser(sql);
    
    console.log("user", user)
    if(user.length === 0){
        result.error = "user doesn't exist"    
        return result;
    }
    
    let checkPassword = bcrypt.compareSync(req.body.password, user[0].passwordhash);
    if(checkPassword){
        result.checkLogin = true;
        return result;
    }
    result.error = "password incorrect!"
    return result;
    
};
