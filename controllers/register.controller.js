const db = require('../database/db_connection')
const bcrypt = require('bcrypt')
const saltRound = 10;

module.exports.postRegister = async function(req, res){
    let regex = /^[a-z1-9]*@([\w\d\.]*)/g
    let result = {};
    if(!regex.test(req.body.username)){
        result.error = "please don't enter special character!"
        return result;
    }
    if(req.body.password.length < 6){
        result.error = "password is too short!"
        return result;
    }

    let hash = bcrypt.hashSync(req.body.password, saltRound)
    let sql = `INSERT INTO users (username, passwordhash) VALUES ('${req.body.username}', '${hash}');`
    console.log(sql)
    let checkInsert = await db.writeUser(sql)
    console.log(checkInsert)
    if(checkInsert) {
        result.checkRegister = true;
    } else {
        result.checkRegister = false;
    }
    return result;
}