const router = require('express').Router()
const loginController = require('../controllers/login.controller');

router.get('/', (req, res) => {
    res.render('login')
})

router.post('/', (req, res) => {
    loginController.postLogin(req, res).then(result => {
        if(result.error){
            res.render('login', { announcement: result.error })
            return 0
        }
        if(result.checkLogin){
            res.render('user')
        } else {
            res.render('login')
        }
    }).catch(err => {
        console.log(err)
    })
})

module.exports = router;