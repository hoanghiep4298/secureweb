const router = require('express').Router()
const registerController = require('../controllers/register.controller')

router.get('/', (req, res) => {
    res.render('register')
})

router.post('/', (req, res) => {
    registerController.postRegister(req, res).then(result => {
        if(result.error){
            res.render('register', { announcement: result.error})
            return
        }

        if(result.checkRegister){
            res.redirect(200, 'login')
            return
        }
        res.render('register', {announcement: "Try again!"})
    }).catch(err => {
        console.log(err)
    })
})
module.exports = router;