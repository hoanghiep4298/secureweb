const mysql = require('mysql')
const util = require('util');

const config = {
    database: "secure",
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '123456'
}


module.exports = {
    getUser: async function(sql){
        const connection = mysql.createConnection(config)
        const query = util.promisify(connection.query).bind(connection);
        try {
            const rows = await query(sql);
            console.log(rows)
            return rows
        } finally{
            connection.end()
        }
        
    },

    writeUser: async function(sql){
        const connection = mysql.createConnection(config)
        const query = util.promisify(connection.query).bind(connection);
        try {
            const rows = await query(sql);
            console.log("loggg", rows)
            if(rows) 
            return true
            else return false
        } 
        catch (e){
            console.log(e)
            return false;
        }
        finally{
            connection.end()
        }
        
    }
}
